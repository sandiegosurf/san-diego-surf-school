Learn to surf with San Diego Surf School. We offer private surf lessons, group surf lessons, rentals, kids surf camps and adult surf camps in San Diego with locations in Pacific Beach and Ocean Beach. Book online today!

Address: 4850 Cass St, San Diego, CA 92109, USA

Phone: 858-205-7683
